import './App.css';
import React, { useState } from 'react';

function Banner() {
  return (
    <h1> Todo application</h1>
  )
}

function ToDoFormAndList() {
  const [itemText, setItemText] = useState(""); 
  const [items, setItems] = useState([{id:1,text:"todo 1"},{id:2, text:"todo 2"}]); 

// add a new item 
const handleSubmit = (event) => {
  event.preventDefault();
  setItems([...items, {id: Math.random(), text: itemText}])
  setItemText("")
}

// remove item
const removeItem = (id) => {
  const newItems = items.filter(item => item.id !== id);
  setItems(newItems);
}

// change item status
const toggleStatus = (id) => {
  const toggleButton = document.getElementById(id);
  if (toggleButton.value === "not done") {
  toggleButton.value = "done";
  toggleButton.innerHTML = "done";
  }
  else {
    toggleButton.value = "not done";
    toggleButton.innerHTML = "not done";
  }
}

  return (
    <div>
      <form onSubmit={handleSubmit}>
      <input type='text' 
       value={itemText} 
       onChange={event => setItemText(event.target.value)} 
       placeholder="Write a new todo here" />
        <input type='submit' value='Add'/>
      </form>
      <table>
        <tbody>
  {items.map(item => (
    <tr key={item.id}>
      <td className="todo">
      {" - "+item.text+" "}
      </td>
      <td>
      <button className="toggle" id={item.id} value="not done" onClick={() => toggleStatus(item.id)}>not done</button>
      </td>
      <td>
      <button className ="delete" onClick={() => removeItem(item.id)}>Delete</button>
      </td>
      </tr>
  ))}
 </tbody>
      </table>
    </div>
  )  
}

function App() {
  return (
    <>
<Banner/>
<ToDoFormAndList/>
    </>
  );
}

export default App;
