import React from "react";
import './App.css';

function FormatNames() {
  const nameList = ["Ben", "Ella", "Anna", "Mark"];
  let count = false;
  return (
  <div>
{nameList.map(function(name) {
  count = !count;
  if (count)
  return <div key={name}><b>{name}</b> </div>
  else
  return <div key={name}><i>{name}</i> </div>
}
)}
    </div>
    )
}

function App() {

  return (
    <div>
      <FormatNames/>
    </div>
  );
}

export default App;
