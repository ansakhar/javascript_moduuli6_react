import React, {useState} from "react";
import './App.css';

function App() {

  const [time, setTime] = useState(Date.now());

function Increase() {
  const [value, setValue] = useState(Date.now());
    setInterval(() => setValue(Date.now()), 1000)
    return (
      <h1>{value.toLocaleTimeString()}</h1>
    )
  }
  
const clock = () => {
  
  const interval = setInterval(() => setTime(Date.now()), 1000);
  return () => {
    clearInterval(interval);
  };

}

clock();
  return (
    <div>
   
      <Increase/>
      <p>Clock: {time}</p>
    </div>
  );
}

export default App;
