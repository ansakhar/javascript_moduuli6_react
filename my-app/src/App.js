import './App.css';


function App() {
  function Display(props) {
    return(
      <h1>{props.text}</h1>
    )
  }

const year = new Date().getFullYear();

function Year() {
  return(
    <h2>{`It's ${year}`}</h2>
  )
}
  return (
    <div>
      <h1>Hello React!</h1>
      <h2>This year {year}</h2>
      <Display text={year}/>
      <Year/>
    </div>
  );
}

export default App;
