import React from "react";

const TodoItem = ({todo, onClick, onRemoveClick}) => {
    return (
        <div className="wrapper" style={{backgroundColor: todo.complete ? "lightgreen" : "pink"}}>
            <h3>{todo.name}</h3>
            <button
                className="btn"
                onClick={onClick}
            >
                { todo.complete ? "Complete" : "Incomplete"}
            </button>
            <button
                className="btn"
                onClick={onRemoveClick}>
                    Remove from List
            </button>    
        </div>

    );
};

export default TodoItem;