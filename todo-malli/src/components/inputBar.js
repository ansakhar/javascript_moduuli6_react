import React, { useState } from "react";

const InputBar = ({ addTodo, generateNewId }) => {
    const [newTodo, setNewTodo] = useState("");

    const insertTodo = (e) => {
        e.preventDefault();
        const newId = generateNewId();
        const newTodoItem = {
            id: newId,
            name: newTodo,
            complete: false
        };
        setNewTodo("");
        addTodo(newTodoItem);
    };

    return (<form
        className="wrapper"
        style={{"grid-template-columns": "7fr 2fr"}}
        onSubmit={insertTodo}>
        <input
            placeholder="Add new todo"
            value={newTodo}
            onChange={(e) => setNewTodo(e.currentTarget.value)}
        />
        <button
            className="btn btn-success"
            type="submit"
        >
                    Submit
        </button>
    </form>
    );
};

export default InputBar;
