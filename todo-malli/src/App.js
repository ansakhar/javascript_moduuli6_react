import React, { useState } from "react";
import "./App.css";

import TodoItem from "./components/todoItem";
import InputBar from "./components/inputBar";

const baseTodos = [
    {id: 1, name: "Go to the supermarket", complete: false},
    {id: 2, name: "Call Alice", complete: false},
    {id: 3, name: "Ask Alice to call Bob", complete: false},
    {id: 4, name: "Do the dishes", complete: false},
    {id: 5, name: "Change car tyres", complete: false}
];

function App() {

    const [todos, setTodos] = useState(baseTodos);

    const generateNewId = () => {
        const newId = Math.random() * 10000 + 1;
        const exists = todos.find((todo) => todo.id === newId);
        return exists ? generateNewId() : newId;
    };

    const toggleComplete = (id) => {
        const todoItem = todos.find((todo) => todo.id === id);
        todoItem.complete = !todoItem.complete;
        setTodos([...todos]);
    };

    const addTodo = (newTodo) => setTodos([...todos, newTodo]);

    const removeTodo = (id) => {
        const newTodos = todos.filter((todo) => todo.id !== id);
        setTodos(newTodos);
    };

    const renderTodos = () => todos.map((todo) => (
        <TodoItem
            key={todo.id}
            todo={todo}
            onClick={() => toggleComplete(todo.id)}
            onRemoveClick={() => removeTodo(todo.id)}
        />
    ));

    return (
        <div className="App">
            {renderTodos()}
            <InputBar
                generateNewId={generateNewId}
                addTodo={addTodo}
            />
        </div>
    );
}

export default App;
