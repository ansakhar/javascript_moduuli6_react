import './App.css';
import React from 'react';
import BooksFormAndList from './BooksFormAndList';
import Banner from './Banner';

function App() {
  return (
    <>
<Banner/>
<BooksFormAndList/>
    </>
  );
}

export default App;
