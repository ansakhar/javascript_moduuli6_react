import React, {useState} from "react";
import { books_array } from "./books_array";
import Book from "./Book";
import { v4 as uuidv4 } from "uuid";

export default function BooksFormAndList() {
    const [books, setBooks] = useState(books_array); 
    const [bookName, setBookName] = useState(""); 
    const [bookAuthor, setBookAuthor] = useState("");
    const [bookStatus, setBookStatus] = useState("");
    const [idSelected, setIdSelected] = useState(null);
    

  
    // add a new book
    const handleAddBook= () => {
        setBooks([...books, {id: uuidv4(), name: bookName, author: bookAuthor, read: bookStatus}]);
        setBookName("");
        setBookAuthor("");
        setBookStatus("");
    };

    // update book
    const handleUpdateBook= (id) => {
        const newBooks = books.map(book => {
            if (book.id === id) {
                book.name = bookName;
                book.author = bookAuthor;
                book.status = bookStatus;
            }
            return book;
        });
        setBooks(newBooks);
        setBookName("");
        setBookAuthor("");
        setBookStatus("");
        toggleStatus(id);
    };
  
    // delete book
    const handleDeleteBook = (id) => {
        const newBooks = books.filter(book => book.id !== id);
        setBooks(newBooks);
        setBookName("");
        setBookAuthor("");
        setBookStatus("");
   

    };
  
    // change book status
    const toggleStatus = (id) => {
        if (idSelected !== null) {
            const resetButton = document.getElementById(idSelected);
            resetButton.value = "not selected";
            resetButton.innerHTML = "not selected";
        }
        const toggleButton = document.getElementById(id);
        if (toggleButton.value === "not selected") {
            toggleButton.value = "selected";
            toggleButton.innerHTML = "selected";
            setIdSelected(id);
            const selectedBook = books.find(book => book.id === id);
            setBookName(selectedBook.name);
            setBookAuthor(selectedBook.author);
            setBookStatus(selectedBook.read);
        }
        else {
            toggleButton.value = "not selected";
            toggleButton.innerHTML = "not selected";
        }
    };

    const booksList = books.map(book => (
        <Book key={book.id} book={book} toggleStatus={toggleStatus} removeItem={handleDeleteBook}/>
    ));

    return (
        <div>
            <table>
                <tbody>
                    <tr>
                        <td>book name:</td>
                        <td><input type="text" value={bookName} onChange={event => setBookName(event.target.value)} placeholder="book name"  required="required"/></td>
                    </tr>
                    <tr>
                        <td>author:</td>
                        <td><input type="text" value={bookAuthor} onChange={event => setBookAuthor(event.target.value)} placeholder="author"/></td>
                    </tr>
                    <tr>
                        <td>status:</td>
                        <td><input type="text" value={bookStatus} onChange={event => setBookStatus(event.target.value)} placeholder="book status"/></td>
                    </tr>
                    <tr>
                        <td><input type="button" value="Add" onClick={() => handleAddBook()}/></td>
                        <td><input type="button" value="Update" onClick={() => handleUpdateBook(idSelected)}/></td>
                        <td><input type="button" value="Delete" onClick={() => handleDeleteBook(idSelected)}/></td>
                    </tr>
                </tbody>
            </table>
            <br></br>
            <h3>Book list:</h3>
            <table>
                <tbody>
                    {booksList}
                </tbody>
            </table>
        </div>
    );  
}