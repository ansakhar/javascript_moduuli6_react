export const books_array =
[
    {
        "name": "Annan book",
        "author": "anybody",
        "read": true,
        "userId": "e7595b76-1a71-403e-ba48-429486787f92",
        "id": 1
    },
    {
        "name": "Annan book",
        "author": "anybody",
        "read": true,
        "userId": "e7595b76-1a71-403e-ba48-429486787f92",
        "id": 2
    },
    {
        "name": "Konstan book",
        "author": "anybody",
        "read": true,
        "userId": "c88481ee-6014-40d3-8756-58bec2b3e0d2",
        "id": 3
    }
];