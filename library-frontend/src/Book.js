import React from "react";

export default function Book({book, toggleStatus}) {
    function handleToggleStatus() {
        toggleStatus(book.id);
    }
    return (
        <tr >
            <td className="bookName"> {book.name}</td>
            <td className="bookName"> {book.author}</td>
            <td> {book.read}</td>
            <td>
                <button className="toggle" id={book.id} value="not selected" onClick={() => handleToggleStatus()}>not selected</button>
            </td>
        </tr>
    );
}
  